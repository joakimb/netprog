package u1_3_1;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class Runner implements Runnable{
	
	String pdfLink;
	
	public Runner(String pdfLink){
		this.pdfLink = pdfLink;
	}

	@Override
	public void run() {
		System.out.println("downloading: " + pdfLink);
		URL url;
		try {
			url = new URL(pdfLink);
		

		InputStream iStream = url.openStream();
		int i = pdfLink.lastIndexOf("/");
		String fileName = pdfLink.substring(i+1);
		File file = new File(fileName);
		file.createNewFile();
		
		byte[] buffer = new byte[4096];
		int n = -1;
		
		FileOutputStream outputStream = new FileOutputStream(file);
		while((n = iStream.read(buffer)) != -1){
			if(n > 0){
				outputStream.write(buffer, 0 , n);
			}
		}
		outputStream.close();
		} catch (IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
