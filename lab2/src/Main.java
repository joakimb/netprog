

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class Main {
	
	public static final String url = "http://cs.lth.se/eda095/foerelaesningar/";
	
	public static void main(String args[]) throws IOException{
		
		//Indexer.download(Indexer.indexPDFFiles("http://cs.lth.se/eda095/foerelaesningar/")); //1_3_1
		Indexer indexer = new Indexer(url);
		indexer.updateFileList();
		
		//1_3_1
		indexer.download(indexer.getFileList());
	}
}
