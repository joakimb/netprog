package lab1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Downloader {
	public static String download(String string){
		URL url = null;
		try {
			url = new URL(string);
			URLConnection conn = url.openConnection();
		    BufferedReader buff = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String tmp = null;
			
			do{
				tmp = buff.readLine();
				if (tmp != null) sb.append(tmp);
			} while(tmp != null);
			return sb.toString();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static String LINK_PATTERN = "<a\\s?href\\s?=\\s?\"(.*?)\""; 
	public static ArrayList<String> extractLinks(String text){
		ArrayList<String> Links = new ArrayList<String>();
		Pattern pattern = Pattern.compile(LINK_PATTERN);
		Matcher matchTag = pattern.matcher(text);
		
		while (matchTag.find()){
			String link = matchTag.group(1);
			Links.add(link);
		}
		
		return Links;
	}
	public static ArrayList<String> extractPDFLinks(String text){
		ArrayList<String> links = extractLinks(text);
		ArrayList<String> PDFLinks = new ArrayList<String>();
		for(String s: links) {
			if(s.contains(".pdf")){
				PDFLinks.add(s);
			}
		}
		return PDFLinks;
	}
	public static void getPDFFiles(String string) throws IOException{
		String s = Downloader.download(string);
		ArrayList<String> PDFLinks = Downloader.extractPDFLinks(s);
		
		for(String pdfLink: PDFLinks){
			System.out.println("downloading: " + pdfLink);
			URL url = new URL(pdfLink);

			InputStream iStream = url.openStream();
			int i = pdfLink.lastIndexOf("/");
			String fileName = pdfLink.substring(i+1);
			File file = new File(fileName);
			file.createNewFile();
			
			byte[] buffer = new byte[4096];
			int n = -1;
			
			FileOutputStream outputStream = new FileOutputStream(file);
			while((n = iStream.read(buffer)) != -1){
				if(n > 0){
					outputStream.write(buffer, 0 , n);
				}
			}
			outputStream.close();
		}
	}
}
